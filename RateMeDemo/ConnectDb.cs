﻿using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using Neo4jClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo
{
    public class ConnectDb
    {
        private static GraphClient client;

        public void Connect()
        {
            if (client != null)
            {
                client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "");
                try
                {
                    client.Connect();
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
            }
        }

    }
}
